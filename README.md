# Ordered Tables Sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DocBook-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DocBook-Compare-5_2_2_j/samples/sample-name`.*

---
## Contents of the sample files

This sample illustrates the mechanism to set up ordered table auto keying so that it can improve a comparison of ordered tables. Although the default comparison behaviour is ordered, it may not be the ordering desired or expected. The auto keying function allows the user to specify the ordering they want for a table comparison. The sample contains three pairs of inputs A and B. In each case the B input differs from the A input by having two rows deleted, and the rows following the deletion have their number column changed so that column one has an incremental number.

1. ordered-table-no-PI: An ordered table with no Processing Instruction (PI). The result is not what we want in this case as we want the order to be based on the Description cells.
2. ordered-table-with-PI-cellpos: An ordered table with the <?dxml-ordered-rows?> PI on the element containing row elements, and <?dxml-key text?> on each row element. The key on each row tells the comparison that we are interested in aligning on column two of this table, the items, and not the number in column one. This gives a result that is ordered, based on the keys we specifed as the Description cells.
3. ordered-table-with-PI-keyed: An ordered table with <?dxml-ordered-rows cell-pos:2?> PI set. This tells the comparison to key on the cells at one-based indexing position two in all the rows of the table. In this example this is effectively column two. This gives the same result as keying individual rows on the text of column two in the previous ordered-table-with-PI-cellpos example, but only requires the parent of the row elements to have a PI. We did not have to explicitly specify a key on each row.

## Running the sample via the Ant build script

If you have Ant available on your path, simply navigate to the sample directory and run the following command:

	ant

The Ant command runs three comparisons, using each of the pairs of input files and producing three result files.

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

It is possible to compare two DocBook files using the command line tool from sample directory as follows. We will use the ordered-table-with-PI-cellpos input files as an example here. The following line can be copied and pasted directly into the Terminal window, when your current directory is ordered-tables.

    java -jar ../../deltaxml-docbook.jar compare ordered-table-with-PI-cellpos-a.xml ordered-table-with-PI-cellpos-b.xml result.xml indent=yes
    
Other parameters are available as discussed in the [User Guide](https://docs.deltaxml.com/docbook-compare/latest/user-guide) and summarised by the following command

    java -jar ../../deltaxml-docbook.jar describe
   

To clean up the sample directory, run the following Ant command.

	ant clean